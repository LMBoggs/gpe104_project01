﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//For Milestone 1, this script allows the character to move at a speed set by the designer (via public variable in the inspector)


public class myPlayerController : MonoBehaviour {

    //[PRIVATE] DECLARE VARIABLES
    private Transform myTransform; //var to hold transform component of game object
    private SpriteRenderer myRenderer; //var to hold this object's sprite renderer
    private bool isPaused; // a private boolean to store if the game is paused or not
    private AudioSource myMusic; //variable to hold audio source attached to this game object
    
    //[PUBLIC] DECLARE VARIABLES
    public float mySpeed; // public float variable for designer to set speed of character
    public GameObject selfGO; //variable to store itself as a game object, so it can be disabled (requirement of Project 1 assignment)
    public Animator myAnimator; // variable to hold animator component
    public GameObject pauseCanvas; // public game object to hold pause canvas object
    public AudioSource pauseJingle; //public variable to store pause jingle audio
    public float pauseJingleVolume; // designer-friendly variable to set volume of pause jingle

    //float horizontalMove = 0f; //float to track horiz. movement, initialized to 0

    //SPECIAL STATUS VARIABLES
    public int playerFacing = 3; //int variable to hold facing up(1), side(2), and down(3), initialized to face down
    public bool isMoving; //stores if the character is moving or not. This is my workaround for animation variables w/o GetAxis Horiz.

	// Use this for initialization
	void Start () {
		//Set myTransform to hold transform on current game object
        myTransform = GetComponent<Transform>();

        //Set myRenderer to hold this object's sprite renderer
        myRenderer = gameObject.GetComponent<SpriteRenderer>();

        //Set the game to NOT be paused on start
	    isPaused = false;

        //set audio source to the component on this game object
        myMusic = GetComponent<AudioSource>();

        //set pause canvas to inactive on start
        pauseCanvas.SetActive(false);

        //set pause jingle volume to designer input
        pauseJingle.volume = pauseJingleVolume;

	    //FUTURE FIX: SAFE CODING
	    //If speed is not set by designer, set default speed
	    /* 
        if (mySpeed == null) //error: float cannot be null, this always yields false <- how can I represent "if mySpeed is not set"or "if mySpeed is blank"?
        {
            mySpeed = 0.5f;
        }
        */
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        //Update isMoving in Animator
        myAnimator.SetBool("isMoving_Anim", isMoving);

        //Update playerFacing in Animator
        myAnimator.SetInteger("playerFacing_Anim", playerFacing);


        //IF THE GAME IS NOT PAUSED, ACCEPT INPUT FROM USER
        //--------------------------------------------------

	    if (isPaused == false)
        {
            //NO OPPOSITE MOVEMENTS
            //---------------------------------------------

            // If both W and S are down, do nothing -- stops character when both are pressed
            if ((Input.GetButton("Up")) && (Input.GetButton("Down")))
            {
                // do nothing (no transform command is called)
                isMoving = false;
            }

            // If both A and D are down, do nothing -- stops character when both are pressed
            else if ((Input.GetButton("Left")) && (Input.GetButton("Right")))
	        {
                // do nothing (no transform command is called)
                isMoving = false;
            }

            //SHIFT CONTROLS
            //---------------------------------------------

            // UP   Move 1 meter up on Shift and Up
            else if ((Input.GetKey(KeyCode.LeftShift)) && (Input.GetButtonDown("Up")))
            {
                myTransform.position = myTransform.position + Vector3.up; //update transform position up 1 meter

                playerFacing = 1; //set facing status to up(1)

                isMoving = true;
            }

            // LEFT   Move 1 meter left on Shift and Left
            else if ((Input.GetKey(KeyCode.LeftShift)) && (Input.GetButtonDown("Left")))
            {
                myRenderer.flipX = true; // flip the sprite to the left

                myTransform.position = myTransform.position + Vector3.left; //update transform position to the left 1 meter

                playerFacing = 2; //set facing status to side(2)

                isMoving = true;
            }

            // DOWN   Move 1 meter left on Shift and Down
            else if ((Input.GetKey(KeyCode.LeftShift)) && (Input.GetButtonDown("Down")))
            {
                myTransform.position = myTransform.position + Vector3.down; //update transform position down 1 meter

                playerFacing = 3; //set facing status to down(3)

                isMoving = true;
            }

            // RIGHT   Move 1 meter right on Shift and Right
            else if ((Input.GetKey(KeyCode.LeftShift)) && (Input.GetButtonDown("Right")))
	        {
	            myRenderer.flipX = false; // flip the sprite to the right

	            myTransform.position = myTransform.position + Vector3.right; //update transform position to the right 1 meter

                playerFacing = 2; //set facing status to side(2)

                isMoving = true;
            }

            //NORMAL CONTROLS
            //-------------------------------------

            // UP   Move Up, but not while L Shift is pressed
            else if (Input.GetButton("Up") && (!Input.GetKey(KeyCode.LeftShift)))
            {                
                myTransform.position = myTransform.position + (Vector3.up * mySpeed); //update transform position up 1 frame per second times speed

                playerFacing = 1; //set facing status to up(1)

                isMoving = true;
            }

            // LEFT   Move Left, but not while L Shift is pressed
            else if (Input.GetButton("Left") && (!Input.GetKey(KeyCode.LeftShift)))
	        {
	            myRenderer.flipX = true; // flip the sprite to the left

	            myTransform.position = myTransform.position + (Vector3.left * mySpeed); //update transform position to the left 1 frame per second times speed

                playerFacing = 2; //set facing status to side(2)

                isMoving = true;
            }

            // DOWN   Move Down, but not while L Shift is pressed
            else if (Input.GetButton("Down") && (!Input.GetKey(KeyCode.LeftShift)))
            {
                myTransform.position = myTransform.position + (Vector3.down * mySpeed); //update transform position down 1 frame per second times speed

                playerFacing = 3; //set facing status to down(3)

                isMoving = true;
            }

            // RIGHT   Move Right, but not while L Shift or A are pressed
            else if ((Input.GetButton("Right")) && (!Input.GetKey(KeyCode.LeftShift)))
            {
                myRenderer.flipX = false; // flip the sprite to the right

                myTransform.position = myTransform.position + (Vector3.right * mySpeed); //update transform position to the right 1 frame per second times speed

                playerFacing = 2; //set facing status to side(2)

                isMoving = true;
                
            }

            //SPECIAL CONTROLS
            //----------------------------------

            // Center to World on SPCBAR
            if (Input.GetKeyDown(KeyCode.Space))
	        {
	            myTransform.position = Vector3.zero; //vector3.zero is the center of the world

                isMoving = false;
            }

            // Pause the game
	        if (Input.GetKeyDown(KeyCode.P))
	        {
	            isPaused = true; //set game to paused

                pauseCanvas.SetActive(true); //activate pause canvas

                isMoving = false;

                myMusic.Pause(); //pause the music

                pauseJingle.Play();

                Debug.Log("PAUSED GAME");
	        }

            //Quit Game Cleanly
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
                Debug.Log("QUIT GAME");
            }

            //Deactivate Self as Game Object
            if (Input.GetKeyDown(KeyCode.Q))
            {
                selfGO.SetActive(false);
                Debug.Log("DEACTIVATED PLAYER OBJECT");
            }

            //Not moving if no controlls are pressed
            if (  (!Input.GetButton("Up")) && (!Input.GetButton("Down")) && (!Input.GetButton("Left")) && (!Input.GetButton("Right")) )
            {
                isMoving = false;
                
            }

        }

        //ELSE IF GAME IS PAUSED, ACCEPT INPUT FROM P KEY TO RESUME THE GAME
	    else if ((isPaused) && (Input.GetKeyDown(KeyCode.P)))
	    {
	        isPaused = false; //unpause the game

            pauseCanvas.SetActive(false); //deactivate pause canvas

            myMusic.UnPause(); //unpause the music

            Debug.Log("RESUMED GAME");

        }

    }
}
