﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class myInteractables : MonoBehaviour {

    //[PRIVATE] DECLARE VARIABLES

    //[PUBLIC] DECLARE VARIABLES
    public BoxCollider2D myTrigger; //public variable to hold trigger collider on player
    public AudioSource itemPickupJingle; //holds audio source for item pickup jingle
    public float itemPickupJingleVolume; //designer-friendly field for item pick up volume

    // Use this for initialization
    void Start () {

        itemPickupJingle.volume = itemPickupJingleVolume; //set volume to designer specified float
	}
	
	// Update is called once per frame
	void Update () {
		
        


	}

    //when object enters the trigger
    private void OnTriggerStay2D(Collider2D other)
    {
        Debug.Log("Object is in trigger [Stay()]");

        //If the player presses the interact button
        if (Input.GetButtonDown("Interact"))
        {
            Debug.Log("Interaction processed");

            //Display Text

            //Play Sound
            itemPickupJingle.Play();


            //Destroy Object
        }

        //POTIONS

        //POKEBALL

        //REPEL
    }

}
